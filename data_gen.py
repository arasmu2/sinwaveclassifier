import numpy as np
import matplotlib.pyplot as plt

SAMPLE_RATE = 48000
MAX_16BIT_S = 32767

FREQ_LOW = 1000
FREQ_HIGH = 20000
FREQ_STEP = 1000
FREQ = FREQ_HIGH / FREQ_STEP

AMP_LOW = 2
AMP_HIGH = 16
AMP_STEP = 2
AMP = AMP_HIGH / AMP_STEP

PHASES = 6

waves = np.zeros((int(FREQ*AMP*PHASES), SAMPLE_RATE))
labels = np.zeros((int(FREQ*AMP*PHASES), 3))

# Compute an array representing a sin wave for a given frequency, amplitude, and phase
def sin_wave(frequency, amplitude, phase):
  wave = np.linspace(0, 1, 48000)
  wave = amplitude * np.sin(2 * np.pi * frequency * wave + (phase/PHASES)*(2*np.pi))
  return wave

i = 0
for f in range(FREQ_LOW, FREQ_HIGH+1, FREQ_STEP): # 20 frequency samples
  for a in range(AMP_LOW, AMP_HIGH+1, AMP_STEP): # 8 amplitude samples
    for p in range(PHASES): # 6 phase samples
      wv = sin_wave(f, 2**a, p)
      # plt.plot(wv)
      # plt.show()
      
      # Normalize values before storing
      waves[i] = wv/MAX_16BIT_S
      labels[i][0] = f/FREQ_HIGH
      labels[i][1] = a/AMP_HIGH
      labels[i][2] = p/PHASES
      i = i + 1

# Save as binary .npz file      
np.savez(f'wave_samples2', waves)
np.savez('wave_labels2', labels)

# Save as CSV file
# np.savetxt(f'wave_samples2.csv', waves)
# np.savetxt(f'wave_labels2.csv', labels)