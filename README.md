# SinWaveClassifier

After speaking with Prof. Massey about an alternate project, he recommended a Neural Network that classifies/identifies sin waves based on their frequency, amplitude, and phase.

## Files

- data_gen.py: Constructs the dataset. Generates 1 second sin wave samples of varying frequency, amplitude and phase, and the accompanying labels for classification.

- model.py: The Neural Nework code. Constructs a three output NN , and trains it on the provided dataset. The dataset is randomly partitioned 70/30 into testing and testing and validation sets. Two different architectures and trining regimes were tested:

  - The first model tested consisted of two shared hidden layers (200 nodes -> 5 nodes) feeding into three discrete outputs. The network is trained in two rounds of 50 epochs each, with the dataset being shuffled between rounds.
  - The second model tested consisted of a single shared hidden layer (100 nodes) feeding into three discrete outputs. The network is trained in one round of 100 epochs.

## Methodology

The original project was aimed at audio file compression but was clearly beyond our capabilities. This is a stepping stone to audio compression. If sin waves can be robustly identified and catalouged, the next step is to perform Short Time Fourier Transforms on audio files, and use machine learning to pluck out the sin waves present at any given moment. These waves can then be stored in a much simpler form, as just their frequency, amplitude, and phase data
