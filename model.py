import numpy as np 
import tensorflow as tf
from keras.models import Model
from keras.layers import Input
from keras.layers.core import Dense
from keras.layers.core import Dropout
import matplotlib.pyplot as plt

# Dataset locations
dataset = 'sinwaveclassifier/wave_samples2.npz'
dataset_labels = 'sinwaveclassifier/wave_labels2.npz'

# Training Parameters
TRAIN_TEST_SPLIT = 0.7
ROUNDS = 1
EPOCHS = 100
INIT_LR = 1e-4
BATCH_SIZE = 32

# Lists to hold training and validation metrics
test_freq_mae = []
test_freq_acc = []
val_freq_mae = []
val_freq_acc = []

test_amp_mae = []
test_amp_acc = []
val_amp_mae = []
val_amp_acc = []

test_phase_mae = []
test_phase_acc = []
val_phase_mae = []
val_phase_acc = []

test_loss = []
val_loss = []

# Generates training and validation data in batches to avoid keeping all data in memory
class WaveDataGenerator():
  
  def __init__(self, ds, ds_l):
    # Memory map the data and label arrays
    self.sin_facts = np.load(ds_l, mmap_mode='r')
    self.sin_waves = np.load(ds, mmap_mode='r')
    
    # Get the shape of the arrays for later use in batch generation
    temp = np.shape(self.sin_waves.f.arr_0)
    self.len = temp[0]
    self.sin_width = temp[1]
    temp = np.shape(self.sin_facts.f.arr_0)
    self.facts_width = temp[1]
    
  # Generate a random permutation of the indices and create testing/validation splitts
  def generate_split_indexes(self):
    p = np.random.permutation(self.len)
    train_up_to = int(self.len * TRAIN_TEST_SPLIT)
    train_idx = p[:train_up_to]
    valid_idx = p[train_up_to:]
    
    return train_idx, valid_idx
  
  # Generate a batch of data and labels
  def generate_batch(self, wave_idx, is_training, batch_size):
    # Arrays to store batched data
    waves = np.zeros((batch_size, self.sin_width))
    freq = np.zeros((batch_size, self.facts_width))
    amp = np.zeros((batch_size, self.facts_width))
    phase = np.zeros((batch_size, self.facts_width))
    
    while True:
      i = 0
      for idx in wave_idx:
        # Fetch the label data from disk
        temp = self.sin_facts.f.arr_0[idx]
        f = temp[0]
        a = temp[1]
        p = temp[2]
        
        # Fetch the sin wave from disk
        w = self.sin_waves.f.arr_0[idx]
        
        freq[i] = f
        amp[i] = a
        phase[i] = p
        waves[i] = w
        
        i = i + 1
        # yielding condition
        if i >= batch_size:
          yield waves, [freq, amp, phase]
          i = 0
          
      if not is_training:
          break
                              
# Constructs a multi-output neural network for classifying sin waves    
class WaveMultiOutputModel():

  # Construct the shared/default hidden layers
  def make_default_hidden_layers(self, inputs):

    x = Dense(100, activation='relu')(inputs)
    # x = Dropout(0.2)(x)
    # x = Dense(5, activation='relu')(x)
    return x

  # Construct the frequency branch
  def build_frequency_branch(self, inputs):

    x = self.make_default_hidden_layers(inputs)
    x = Dense(1, activation='linear', name='freq_output')(x)
    return x
    
  # Construct the amplitude branch
  def build_amplitude_branch(self, inputs):

    x = self.make_default_hidden_layers(inputs)
    x = Dense(1, activation='linear', name='amp_output')(x)
    return x
    
  # Construct the phase branch
  def build_phase_branch(self, inputs):   

    x = self.make_default_hidden_layers(inputs)
    x = Dense(1, activation='linear', name='phase_output')(x)
    return x
  
  # Construct the complete model
  def assemble_full_model(self):

    input_shape = (48000)
    inputs = Input(shape=input_shape)
    freq_branch = self.build_frequency_branch(inputs)
    amp_branch = self.build_amplitude_branch(inputs)
    phase_branch = self.build_phase_branch(inputs)
    model = Model(inputs=inputs,
                  outputs = [freq_branch, amp_branch, phase_branch],
                  name="wave_net")
    return model

# Build the neural network
model = WaveMultiOutputModel().assemble_full_model()

# Compile model with given optimizer, loss, metric functions
opt = tf.keras.optimizers.Adam(learning_rate=INIT_LR, decay=INIT_LR/EPOCHS)
model.compile(optimizer=opt, 
              loss={'freq_output': 'mse', 
                    'amp_output': 'mse', 
                    'phase_output': 'mse'},
              metrics={'freq_output': ['mae', 'accuracy'], 
                       'amp_output': ['mae', 'accuracy'],
                       'phase_output': ['mae', 'accuracy']})

# Create the data generatos
data_generator = WaveDataGenerator(dataset, dataset_labels)

# Train the model for the given number of rounds, shuffling the data between each round
# Total training time = ROUNDS x EPOCHS
for i in range(ROUNDS):
  # Generate random permutation the data and the correponding batch generators
  train_idx, valid_idx = data_generator.generate_split_indexes()   
  train_gen = data_generator.generate_batch(train_idx, is_training=True, batch_size=BATCH_SIZE)
  valid_gen = data_generator.generate_batch(valid_idx, is_training=True, batch_size=BATCH_SIZE)
  
  # Train the model
  history = model.fit(train_gen,
                      steps_per_epoch=len(train_idx)//BATCH_SIZE,
                      epochs=EPOCHS,
                      validation_data=valid_gen,
                      validation_steps=len(valid_idx)//BATCH_SIZE)
  
  # Save the training/validation metrics across rounds
  for e in range(EPOCHS):
    test_freq_mae.append(history.history['freq_output_mae'][e])
    val_freq_mae.append(history.history['val_freq_output_mae'][e])
    
    test_freq_acc.append(history.history['freq_output_accuracy'][e])
    val_freq_acc.append(history.history['val_freq_output_accuracy'][e])
    
    test_amp_mae.append(history.history['amp_output_mae'][e])
    val_amp_mae.append(history.history['val_amp_output_mae'][e])
    
    test_amp_acc.append(history.history['amp_output_accuracy'][e])
    val_amp_acc.append(history.history['val_amp_output_accuracy'][e])
    
    test_phase_mae.append(history.history['phase_output_mae'][e])
    val_phase_mae.append(history.history['val_phase_output_mae'][e])
    
    test_phase_acc.append(history.history['phase_output_accuracy'][e])
    val_phase_acc.append(history.history['val_phase_output_accuracy'][e])
    
    test_loss.append(history.history['loss'][e])
    val_loss.append(history.history['val_loss'][e])

# Plot the training/validation error and accuracy for each feature
plt.plot(test_freq_mae, label='Train')
plt.plot(val_freq_mae, label = 'Val')
plt.legend()
plt.title('Frequency MAE')
plt.xlabel('Epochs')
plt.ylabel('Mean Absolute Error')
plt.show()
plt.savefig(f'frequency_mae', bbox_inches='tight')
plt.close()

plt.plot(test_freq_acc, label='Train')
plt.plot(val_freq_acc, label = 'Val')
plt.legend()
plt.title('Frequency Accuracy')
plt.xlabel('Epochs')
plt.ylabel('Accuracy')
plt.show()
plt.savefig(f'frequency_acc', bbox_inches='tight')
plt.close()

plt.plot(test_amp_mae, label='Train')
plt.plot(val_amp_mae, label = 'Val')
plt.legend()
plt.title('Amplitude MAE')
plt.xlabel('Epochs')
plt.ylabel('Mean Absolute Error')
plt.show()
plt.savefig(f'amplitude_mae', bbox_inches='tight')
plt.close()

plt.plot(test_amp_acc, label='Train')
plt.plot(val_amp_acc, label = 'Val')
plt.legend()
plt.title('Amplitude Accuracy')
plt.xlabel('Epochs')
plt.ylabel('Accuracy')
plt.show()
plt.savefig(f'amplitude_acc', bbox_inches='tight')
plt.close()

plt.plot(test_phase_mae, label='Train')
plt.plot(val_phase_mae, label = 'Val')
plt.legend()
plt.title('Phase MAE')
plt.xlabel('Epochs')
plt.ylabel('Mean Absolute Error')
plt.show()
plt.savefig(f'phase_mae', bbox_inches='tight')
plt.close()

plt.plot(test_phase_acc, label='Train')
plt.plot(val_phase_acc, label = 'Val')
plt.legend()
plt.title('Phase Accuracy')
plt.xlabel('Epochs')
plt.ylabel('Accuracy')
plt.show()
plt.savefig(f'phase_acc', bbox_inches='tight')
plt.close()

# Plot the overall loss across training
plt.plot(test_loss, label='Train')
plt.plot(val_loss, label = 'Val')
plt.legend()
plt.title('Overall Loss')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.show()
plt.savefig(f'loss', bbox_inches='tight')
plt.close()